# ARISE - A Multi-Task Weak Supervision Framework for Network Measurements

Code base for IEEE JSAC 2022 paper, [ARISE: A Multi-Task Weak Supervision Framework for Network Measurements](https://gitlab.com/onrg/arise).
The source code is available [here](https://gitlab.com/onrg/arise).

<details><summary style="font-size:1.5em">Installation</summary>
    <p>To build models with ARISE, you will need Conda and a Linux environment running Ubuntu 20.04. These instructions assume you already have conda installed.</p>
    <p>
    First, create the ARISE environment and configure it for use in JupyterLab. From the root of this repository, execute the following: (this may take some time)
    </p>
    <p><code>conda env create --name arise --file=arise-environment.yml</code></p>
    <p>Then, activate the virtual environment with <code>conda activate arise</code></p>
    <p>Install the remaining packages with <code>pip install -r arise-requirements.txt</code> (If any packages fail to install, you may need to resolve them manually using <code>pip</code>, <code>conda install</code>, etc.)</p>
    <p>Install the Jupyter Notebook kernel for the current virtual environment with <code>python3 -m ipykernel install --user --name arise --display-name "ARISE"</code></p>
    <p>Now, you should be ready to run the interactive notebook for ARISE, which you can do by executing <code>jupyter lab</code> in the root of the repository.</p>
    <p>To configure the conda environment to run the EMERGE STL or NoMoNoise naïve methods, please see the installation instructions in the <a href="https://gitlab.com/onrg/emerge">EMERGE repository</a>.</p>
</details>

<details><summary style="font-size:1.5em">Usage</summary>
    <p>To build models with ARISE, navigate to the <code>Multitask.ipynb</code> notebook in the <code>MTL/</code> directory. This notebook is configured to load data from either the RIPE Atlas or CAIDA Ark datasets in any given iteration. To adjust the labeling functions or datasets used, modify the notebook cells or data in the paths provided.</p>
    <p>To visualize the results of ARISE, cache the results during model training in the <code>MTL/data/</code> directory and display with inside the <code>MTL/Visualizations.ipynb</code> notebook.</p>
</details>

<details><summary style="font-size:1.5em">Notes</summary>
<ul>
    <li>
        The datasets used in this paper were sourced from the <a href="https://www.caida.org/projects/ark/topo_datasets/">CAIDA Ark</a> and <a href="https://atlas.ripe.net/">RIPE Atlas</a> projects.
        We include the entire subset of measurements from the CAIDA datasets here in <code>~/data_28/</code>, but only the first 100 subsets of data from the RIPE Atlas project in <code>~/ripe</code>, as the dataset is over 10GB in size. 
        The original dataset can be found using the RIPE Atlas Measurements API, and similar datasets for the past 30 days can be found in the <a href="https://data-store.ripe.net/datasets/atlas-daily-dumps/">RIPE Atlas Daily Dumps</a>.
    </li>
    <li>
        Many of the notebooks and subdirectories in this repository are intended for use only in the naive and single-task experiments conducted to establish a baseline for comparison. If attempting to implement a model ARISE, focus should be placed primarily in the data and <code>MTL</code> subdirectories.
    </li>
</ul>
</details>

<details><summary style="font-size:1.5em">Acknowledgements</summary>
<p>This work is supported by the National Science Foundation through CNS 1850297, CNS 2145813, and OAC 2126281, and by a University of Oregon VPRI Fellowship. The views and conclusions contained herein are those of the authors and should not be interpreted as necessarily representing the official policies or endorsements, either expressed or implied, of NSF or the University of Oregon.</p>
<p>We thank the anonymous reviewers for their insightful feedback. We also thank the authors of <a href="https://www.snorkel.org/">Snorkel</a>, <a href="https://gitlab.com/onrg/emerge">EMERGE</a>, and <a href="https://ieeexplore.ieee.org/document/8999330">NoMoNoise</a> for open-sourcing their works to the community.</p>
</details>
