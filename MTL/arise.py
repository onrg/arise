from tsfresh import extract_features, extract_relevant_features
import sys, os, math, random, glob, torch, logging
from pprint import pprint
import numpy as np
import pandas as pd
from sklearn import *
import warnings # see https://docs.python.org/3/library/warnings.html
from warnings import *
warnings.filterwarnings("default") # can also be set to 'ignore'
from utils import *

from snorkel.classification import DictDataset, DictDataLoader, Operation, Task, MultitaskClassifier, Trainer
import torch.nn as nn
import torch.nn.functional as F
from functools import partial
from snorkel.analysis import Scorer

from timeit import default_timer as timer

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)
log = logging.getLogger('arise')
log.setLevel(logging.DEBUG) # can be DEBUG, INFO, WARNING, ERROR, CRITICAL; logger will show all messages BELOW specified level

SEED = 42
np.random.seed(SEED)
random.seed(SEED)
torch.manual_seed(SEED)

DATA_DIR = get_full_path("data_28") + "/"
FIGURE_DIR = get_full_path("MTL", "Figures") + "/"
RIPE_DIR = get_full_path("ripe") + "/"
CUDA_INDEX = 0 # -1 to disable CUDA (force CPU), use 0..2 for Mitra

METRICS = ["f1", "accuracy"]
LOSS_FUNC = F.cross_entropy


# Device Configuration
if torch.cuda.is_available() and CUDA_INDEX >= 0:
    device = torch.device(f'cuda:{CUDA_INDEX}')
    torch.cuda.set_device(CUDA_INDEX)
else:
    log.warning("Defaulting to CPU calculations.")
    device = torch.device('cpu')

log.debug(f"Using {device}.")

#### TSFRESH Feature Extraction

from tsfresh.feature_extraction.feature_calculators import set_property
from tsfresh.feature_extraction import feature_calculators

@set_property("fctype", "simple")
def count_nonzero(x):
    """ Returns the number of nonzero (non-loss) measurements in the time series x. """
    return np.count_nonzero(x)

@set_property("fctype", "simple")
def noise_threshold(x):
    """ Returns the noise threshold for a time series
        by taking 1.5 * the RTT value at the 75th percentile. """
    x = x[x != 0] # remove all loss points from consideration
    return np.percentile(x, 75) * 1.5

@set_property("fctype", "simple")
def congestion_threshold(x):
    """ Returns the congestion threshold for a time series
        by taking 1.2 * the RTT value at the 30th percentile. """
    x = x[x != 0] # remove all loss points from consideration
    return np.percentile(x, 30) * 1.2


# Add custom features to list of feature calculators:
# https://tsfresh.readthedocs.io/en/latest/text/feature_extraction_settings.html
feature_calculators.__dict__["count_nonzero"] = count_nonzero
feature_calculators.__dict__["noise_thresh"] = noise_threshold
feature_calculators.__dict__["congestion_thresh"] = congestion_threshold

custom = {
    "quantile": [{"q": 0.75}],
    #     "length": None, # number of entries in each time series
    "median": None,
    "mean": None,
    "noise_thresh": None,
    "congestion_thresh": None
}
disable_progress_bar = True

#### BUILDING LABELING FUNCTIONS

VOTE    =  1
NORMAL  =  0
ABSTAIN = -1

def label_noise(rtt, client_index):
    noise_threshold = features['rtt__noise_threshold'][client_index]
    if rtt >= noise_threshold:
        label = VOTE
    else:
        label = NORMAL
    return label

def label_outage(rtt, client_index):
    if rtt == 0:
        label = VOTE
    else:
        label = NORMAL
    return label

def label_congestion(rtt, client_index):
    cong_threshold = features['rtt__congestion_threshold'][client_index]
    noise_threshold = features['rtt__noise_threshold'][client_index]

    if rtt >= cong_threshold and rtt < noise_threshold:
        label = VOTE
    else:
        label = NORMAL
    return label

def generate_data(df):
    """ DF should be a labeled, original dataset. """
    global features

    newdata = pd.DataFrame(columns=["id", "i", "rtt", "loss", "congestion", "noise"])

    for i, row in df.iterrows():
        # print(f"Index {i}; data: \n{row['rtt']}\n")
        ID = row['id']

        try:
            index = row['i']
        except:
            index = row['index']

        # Appending original data
        rtt = row['rtt']
        l, c, n = (row['loss'], row['congestion'], row['noise']) # labels
        newdata = newdata.append({'id': ID, 'i': index, 'rtt': rtt, 'loss': l, 'congestion': c, 'noise': n}, ignore_index=True)

        # Appending synthesized data -- loss
        rtt = 0
        l, c, n = (VOTE, NORMAL, NORMAL) # labels
        newdata = newdata.append({'id': ID, 'i': index, 'rtt': rtt, 'loss': l, 'congestion': c, 'noise': n}, ignore_index=True)

        # Appending synthesized data -- congestion
        cong_threshold = features['rtt__congestion_threshold'][ID]
        noise_thresh = features['rtt__noise_threshold'][ID]
        rtt = np.random.randint(cong_threshold, noise_thresh)
        l, c, n = (NORMAL, VOTE, NORMAL) # labels
        newdata = newdata.append({'id': ID, 'i': index, 'rtt': rtt, 'loss': l, 'congestion': c, 'noise': n}, ignore_index=True)

        # Appending synthesized data -- noise
        noise_scale = 5 # scale upper bound for noise by this. original is 4.
        gap = 500 # ensure a gap of at least XXms between synthetic congestion and noise RTT values
        noise_thresh = features['rtt__noise_threshold'][ID]
        rtt = np.random.randint(noise_thresh + gap, noise_thresh * noise_scale + gap)
        l, c, n = (NORMAL, NORMAL, VOTE) # labels
        newdata = newdata.append({'id': ID, 'i': index, 'rtt': rtt, 'loss': l, 'congestion': c, 'noise': n}, ignore_index=True)

    return newdata.reset_index(drop=True)

def ripe_n(n : int):
    ''' Returns an aggregated DataFrame object containing data from the first n RIPE Atlas datasets in ../ripe/ '''
    master = pd.DataFrame(columns=['id', 'index',  'af', 'datetime', 'rtt']) # Create empty dataframe to append new DataFrames to.

    for i in range(n):
        f = RIPE_DIR + f"out_{i:03d}.csv"
        df = pd.read_csv(f)
        df.rename(columns = {"time": "datetime", "avg": "rtt"}, inplace = True) # rename index column to 'index'
        df.reset_index(inplace=True)
        df.insert(0, 'id', i) # create and populate ID column as first entry in dataframe
        df.drop(['Unnamed: 0'], axis=1, inplace=True)
        df = df.astype({'af': int})

        # replace occurrences of RTT == -1 with RTT = 0 to remain consistent with previous LFs
        df.loc[(df.rtt == -1), 'rtt'] = 0

        master = master.append(df, ignore_index = True) # append individual sets to master DF

    master['rtt'] = master['rtt'].round(5) # round to 5 decimals places
    master.drop(['af'], axis=1, inplace=True) # delete the address family (IPv4 vs IPv6) from dataframe
    return master


def setup(tasks : list, df, num_entries):

    log.debug(f"Loaded dataset containing {len(df)} entries.")

    """ ----- Select dataframe to extract features on ----- """


    # Uncomment to load raw RIPE Atlas Data
    # ripe = ripe_n(100) # load all 100 RIPE atlas datasets (~2.5 million data points)
    # ripe.to_csv(ROOT_DIR + "/MTL/data/RIPE Atlas Dataset.csv", encoding='utf-8', header=True)

    # df = master
    # df = ripe_n(100)

    """ Uncomment to use RIPE Atlas Data. """
    # df = pd.read_csv("data/RIPE Atlas Dataset.csv")

    # Perform feature extraction with custom settings
    df.reset_index(inplace=True)


    features = extract_features(df, default_fc_parameters=custom, column_id="id", \
                                column_sort="index", column_value="rtt", \
                                disable_progressbar=disable_progress_bar).round(5)

    #### SPLIT INTO TRAINING/TESTING/VALIDATION SETS

    # X values are the measurements (rtt), Y values are the associated labels
    X_train, X_validate, X_test = {}, {}, {}
    Y_train, Y_validate, Y_test = {}, {}, {}

    # dataframe = raw # raw CAIDA data
    # dataframe = synth
    # num_entries = 28 # 28 entries in CAIDA Ark Dataset

    for i in range(num_entries):
        master = get(df, index=i)

        for task in tasks:
            splt = split_df(master)
            splt_2 = split_df(master, ct=4)
            splt_2 = splt_2[1].append(splt_2[2], ignore_index=True)

            X_train[task] = splt[0]['rtt'].apply(lambda x : np.array([x]))
            X_validate[task] = splt[1]['rtt'].apply(lambda x : np.array([x]))
            X_test[task] = splt_2['rtt'].apply(lambda x : np.array([x]))

            Y_train[task] = splt[0][task]
            Y_validate[task] = splt[1][task]
            Y_test[task] = splt_2[task]

        rtts = master['rtt'].apply(lambda x : np.array([x])) # convert rtt's into np array of sample arrays



SCORES = {}


def train_and_evaluate(df, i : int, tasks : list, show_conf_matrix=False, n_epochs=10, lr=0.01, directory="", savefig=False, \
                       cache_model=True, titleprefix="", show_progress=False, model_name="MTL_Latest", \
                       use_cached=False, time_override=False):
    """ Iteratively train and evaluate the ith synthesized dataset. Results are stored in the SCORES dictionary, rather
        than being returned. """
    global SCORES, METRICS, LOSS_FUNC, SEED
    CACHE_DIR = "./cache/"

    X_train, X_validate, X_test = {}, {}, {}
    Y_train, Y_validate, Y_test = {}, {}, {}

    master = get(df, index=i)
    for task in tasks:
        splt = split_df(master)
        splt_2 = split_df(master, ct=4)
        splt_2 = splt_2[1].append(splt_2[2], ignore_index=True)

        X_train[task] = splt[0]['rtt'].apply(lambda x : np.array([x]))
        X_validate[task] = splt[1]['rtt'].apply(lambda x : np.array([x]))
        X_test[task] = splt_2['rtt'].apply(lambda x : np.array([x]))

        Y_train[task] = splt[0][task]
        Y_validate[task] = splt[1][task]
        Y_test[task] = splt_2[task]

    rtts = master['rtt'].apply(lambda x : np.array([x])) # convert rtt's into np array of sample arrays

    """ Now, we define the dataloaders for the model. These will access the input data dictionaries for each
        individual dataset and load the required portions needed for validation, training, and testing evaluations. """

    loaders = {t : [] for t in ["train", "valid", "test"] + tasks} # used for confusion matrix generation
    dataloaders = []

    for task_name in tasks:
        for split, X, Y in (
                ("train", X_train, Y_train),
                ("valid", X_validate, Y_validate),
                ("test", X_test, Y_test),
            ):

            X_dict = {f"{task_name}_data": torch.FloatTensor(X[task_name])}
            Y_dict = {f"{task_name}_task": torch.LongTensor(Y[task_name])}

            dataset = DictDataset(f"{task_name}_Dataset", split, X_dict, Y_dict)
            dataloader = DictDataLoader(dataset, batch_size=32) # batch size is the number of data points loaded in at time
            dataloaders.append(dataloader)

            loaders[split].append(dataloader)     # add current loader to list sorted by data type (test, train, or val)
            loaders[task_name].append(dataloader) # add current loader based on task

    """ Here, we define the initial layers of the perceptron model. 'base_mlp' indicates the layer shared between the
        different tasks, while the 'head_module' denotes the model's prediction layer. """

    # Define a two-layer MLP module and a one-layer prediction "head" module
    # base_mlp = nn.Sequential(nn.Linear(2, 8), nn.ReLU(), nn.Linear(8, 4), nn.ReLU())

    numLayers = 4 # default 4
    hiddenSize = 4 # default 4
    in_features = 1

    base_mlp = nn.Sequential(
                    nn.Linear(in_features, hiddenSize),
                    nn.ReLU(),
                    # nn.LSTM(in_features, hidden_size=hiddenSize, num_layers=1, bidirectional=True), # put these first?
                    # nn.ReLU(),
                    nn.Linear(hiddenSize, 4),
                    nn.ReLU()
                )

    # base_mlp = nn.Sequential(
    # nn.LSTM(input_size=1, hidden_size=numLayers, num_layers=1),
    # nn.ReLU(),
    # #                     nn.Linear(in_features, numLayers),
    # #                     nn.ReLU(),
    # nn.Linear(numLayers, 4),
    # nn.ReLU()
    # )
    #
    # head_module = nn.Linear(4, 2)
    # # The module pool contains all the modules this task uses
    # module_pool = nn.ModuleDict({"base_mlp": base_mlp, "loss_head_module": head_module})
    #
    # # From the input dictionary, pull out 'loss_data' and send it through input_module
    # op1 = Operation( name="base_mlp", module_name="base_mlp", inputs=[("_input_", "loss_data")] )
    # # Pass the output of op1 (the MLP module) as input to the head_module
    # op2 = Operation( name="loss_head", module_name="loss_head_module", inputs=["base_mlp"] )
    # op_sequence = [op1, op2]


    """ Here, we define the tasks themselves. These are the layers of the MTL model that share information through
        the 'base_mlp' perceptron module. The tasks function equivalently, and are merely described differently as a result
        of adherence to the Snorkel Multi-Task learning documentation formatting. """

     # loss_task = Task(
        #  name="loss_task",
        #  module_pool=module_pool,
        #  op_sequence=op_sequence,
        #  loss_func=LOSS_FUNC, # defaults to F.cross_entropy
        #  output_func=partial(F.softmax, dim=1),
        #  scorer=Scorer(metrics=METRICS),
     # )
     #
     # noise_task = Task(
        #  name="noise_task",
        #  module_pool=nn.ModuleDict({"base_mlp": base_mlp, "noise_head": nn.Linear(4, 2)}),
        #  loss_func=LOSS_FUNC, # defaults to F.cross_entropy
        #  output_func=partial(F.softmax, dim=1),
        #  scorer=Scorer(metrics=METRICS),
        #  op_sequence=[
        #      Operation("base_mlp", [("_input_", "noise_data")]), # base multi-layered perception
        #      Operation("noise_head", ["base_mlp"]),
        #  ]
     # )
     #
     # congestion_task = Task(
        #  name="congestion_task",
        #  module_pool=nn.ModuleDict({"base_mlp": base_mlp, "congestion_head": nn.Linear(4, 2)}),
        #  loss_func=LOSS_FUNC, # defaults to F.cross_entropy
        #  output_func=partial(F.softmax, dim=1),
        #  scorer=Scorer(metrics=METRICS),
        #  op_sequence=[
        #      Operation("base_mlp", [("_input_", "congestion_data")]), # base multi-layered perception
        #      Operation("congestion_head", ["base_mlp"]),
        #  ]
     # )
     #
     # changepoint_task = Task(
        #  name="changepoint_task",
        #  module_pool=nn.ModuleDict({"base_mlp": base_mlp, "changepoint_head": nn.Linear(4, 2)}),
        #  loss_func=LOSS_FUNC, # defaults to F.cross_entropy
        #  output_func=partial(F.softmax, dim=1),
        #  scorer=Scorer(metrics=METRICS),
        #  op_sequence=[
        #      Operation("base_mlp", [("_input_", "changepoint_data")]), # base multi-layered perception
        #      Operation("changepoint_head", ["base_mlp"]),
        #  ]
     # )

    def initialize_task(taskname : str, base):
        """ A more modular method of defining the same tasks as seen above. """
        global LOSS_FUNC, METRICS

        in_features = 4  # number of elements in hidden layer
        out_features = 2

        task = Task(
            name = f"{taskname}_task",
            module_pool = nn.ModuleDict({"base_mlp": base, f"{taskname}_head": nn.Linear(in_features, out_features)}),
            loss_func = LOSS_FUNC,
            output_func = partial(F.softmax, dim=1),
            scorer = Scorer(metrics=METRICS),
            op_sequence = [
                    Operation("base_mlp", [("_input_", f"{taskname}_data")]), # base multi-layered perception
                    Operation(f"{taskname}_head", ["base_mlp"]),
                ]
            )

        return task

    # loss_task = initialize_task("loss", base_mlp)
    # noise_task = initialize_task("noise", base_mlp)
    # congestion_task = initialize_task("congestion", base_mlp)
    # changepoint_task = initialize_task("changepoint", base_mlp)
    # ddos_task = initialize_task("ddos", base_mlp)

    TASKS = [initialize_task(t, base_mlp) for t in tasks]
    # TASKS = [loss_task, noise_task, congestion_task] # original tasks

    """ Finally, we train the Multi Task classifier and score it according to our evaluation metrics list described above.
        By default, we use the model's F1 score as the basis for evaluation. The duration of the training process for each
        individual dataset within the model is also recorded. """

    model = MultitaskClassifier(TASKS, name="ARISE_Classifier", device=CUDA_INDEX)

    trainer_config = { # see https://github.com/snorkel-team/snorkel/blob/master/snorkel/classification/training/trainer.py
        "seed": SEED,
        "n_epochs": n_epochs,
        "lr": lr,
        "lr_scheduler": "linear", # one of ["constant", "linear", "exponential", "step"]
        "progress_bar": show_progress,
        "checkpointing": True
    }
    trainer = Trainer(**trainer_config)

    if use_cached:
        try:
            model.load(CACHE_DIR + model_name)
        except:
            use_cached = False # if we fail to load config, simply regen model config
            print(f"Error loading cached model {model_name}")


    start = timer() # get current time for measurement analysis
    trainer.fit(model, dataloaders)
    end = timer()

    model.eval()

    results = model.score(dataloaders, as_dataframe=False)
    # print(model.eval())
    # print('model.score:', results)

    try:
        if time_override:
            assert False
        if "Time" in SCORES[i].keys(): # if this is a second iteration:
            SCORES[i]["Time"] = round(SCORES[i]["Time"] + round(end - start, 5), 5)
            SCORES[i]["Scores"] = results
            SCORES[i]["Iterations"] += 1
    except:
        SCORES[i] = {"Time": round(end - start, 5), "Scores": results, "Index": i, "Iterations": 0}

    """  Cache MTL Model to specified filepath: """

    if cache_model:
        model.save(CACHE_DIR + model_name)


    """ Generating Confusion Matrices: """

    typ = "test" # test, train, or valid; which set of the data should be used to generate the confusion matrices?
    result_list = []

    for dl in loaders[typ]:
        res = model.predict(dl, return_preds=True)
        result_list.append(res)

        # print('model.predict:', res)
        # print(res['preds'].keys())

        key = list(res['preds'].keys())[0]

        preds = res["preds"][f"{key}"] # predictions

        try:
            if show_conf_matrix:
                weak_labels = dl.dataset.Y_dict[f"{key}"].numpy() # convert tensor to np array
                conf = gen_confusion_matrix(preds, weak_labels, index=f"{i:02d}", filetype="png", save=savefig, \
                                     selected_feature=key.split('_')[0], pre=titleprefix, useTitle=True, folder=directory)
        except:
            print(f"Error drawing confusion matrix for {i:02d}: {key}")
            pass

    # """ Printing hidden model layers. """
    # l = [module for module in model.modules() if not isinstance(module, nn.Sequential)]
    # print(l)
    #
    # print(list(model.modules()))
    #
    # print("=" * 50)
    # print(f"model: {model}")
    # print(dir(model))
    # print(model.modules)
    # print(model.add_module)
    # print(model.add_task)
    return result_list


def train(tasks, df, datasets=[0], learning_rate=0.01, epochs=10):
    global SCORES, SUPPORTED_TASKS
    avgs = []


    for i in datasets:
        print(f"{'='*5} Training on dataset {i:02d} {'='*5}")

        # cache = True # use cached model
        cache = False
        save_cache = False

        # CAIDA -- train on augmented, test on original/raw
        results = train_and_evaluate(df, i, tasks, savefig=False, show_conf_matrix=False, \
                                    n_epochs=epochs, lr=learning_rate, cache_model=save_cache, \
                                    show_progress=True, use_cached=cache, model_name=f"{i:02d}_MTL")

        # RIPE
        # results = train_and_evaluate(ripe, i, tasks, savefig=False, show_conf_matrix=True, \
        #                              n_epochs=EPOCHS, lr=learning_rate, titleprefix="ripe_", time_override=True, \
        #                              show_progress=True, use_cached=cache, cache_model=save_cache, \
        #                              model_name=f"RIPE_{i:02d}_MTL_change", directory="ripe")

        # RIPE TMP (new changepoint)
        # results = train_and_evaluate(tmp, i, tasks, savefig=False, show_conf_matrix=True, \
        #                     n_epochs=EPOCHS, lr=learning_rate, titleprefix="ripe_", time_override=True, \
        #                     show_progress=True, use_cached=cache, cache_model=save_cache, \
        #                     model_name=f"RIPE_{i:02d}_tmp", directory="ripe")

        # Raw CAIDA data
        # results = train_and_evaluate(raw, i, tasks, savefig=False, show_conf_matrix=False, \
        #                             n_epochs=EPOCHS, lr=rate, titleprefix="raw_", cache_model=save_cache, \
        #                             show_progress=True, use_cached=cache, model_name=f"{i:02d}_MTL_raw")

    avg = 0
    time = 0

    pprint(SCORES)

    for i in SCORES:
        for j in SCORES[i]['Scores']:
            avg += SCORES[i]['Scores'][j] / len(SCORES[i]['Scores'])

        time += SCORES[i]["Time"]

    avg /= len(SCORES)
    avgs.append(avg)
    print(f"\nMean F1 score: `{avg}`  ")
    # print(f'Total time: `{round(time, 5)}` seconds.\n')
    print(f"Average Time: `{round(time / len(SCORES), 5)}`")

    print("Done.")

    return results




def run_default(tasks):
    dataset = "synth" # choose from "raw", "synth", or "ripe"
    
    # datafile = "data/synth_gap500.csv"
    datafile = "arise/MTL/data/synth_gap500.csv"

    try: # Read synth data from CSV by default, otherwise, synthesize it again.
        df = pd.read_csv(datafile, index_col=0)
    except:
        raise Exception(f"{datafile} not found.")

        
    setup(tasks, df, num_entries=28)

    results = train(tasks, df, datasets=range(1), learning_rate=0.01, epochs=3)
    
    return


if __name__ == '__main__':
    tasks = ["loss", "noise", "congestion"]
    # tasks = ["loss", "noise", "congestion", "changepoint", "query1", "query2"]

    dataset = "synth" # choose from "raw", "synth", or "ripe"

    if dataset == "raw":
        df = get_all_datasets() # from utils.py
    elif dataset == "synth":
        datafile = "data/synth_gap500.csv"

        try: # Read synth data from CSV by default, otherwise, synthesize it again.
            df = pd.read_csv(datafile, index_col=0)
        except:
            log.warning(f"{datafile} not found; regenerating.")

            df = master
            new_df = pd.DataFrame(columns=['id', 'i', 'datetime', 'rtt']) # empty dataframe to append modified DataFrames to

            for client_id in range(28):
                """ Synthesize data for better performance with weak supervision. """
                print(f"Synthesizing data for client {client_id:02d}...")

                current_df = df[df['id'] == client_id] # df containing only values for current client
                noise_threshold = features['rtt__noise_threshold'][client_id]
                cong_threshold = features['rtt__congestion_threshold'][client_id]

                # Apply labeling functions to original datasets.
                current_df['loss'] = current_df['rtt'].apply(lambda x: label_outage(x, client_id))
                current_df['congestion'] = current_df['rtt'].apply(lambda x: label_congestion(x, client_id))
                current_df['noise'] = current_df['rtt'].apply(lambda x: label_noise(x, client_id))
                current_df.rename(columns = {'index':'i'}, inplace=True)
                del current_df['datetime']

                # Synthesize new data in line with the current df.
                current_df = generate_data(current_df)

                new_df = new_df.append(current_df) # append modified dataframes to new modified set

            new_df = new_df.astype({"id": 'int', "i": 'int', "loss": 'int', \
                                    "congestion": 'int', "noise": 'int'}) # trim labels to ints, rather than floats

            synth = new_df.reset_index(drop=True)
            del synth["datetime"]
            print("Done.")

            synth.to_csv(datafile, encoding='utf-8', header=True)
    elif dataset == "ripe":
        # df = ripe_n(100)
        df = ripe_n(10)
    else:
        raise Exception("Invalid dataset!")


    setup(tasks, df, num_entries=28)

    train(tasks, df, datasets=range(1), learning_rate=0.01, epochs=3)
