"""
Load data, calculate feature combinations, split data
"""


import numpy as np
import scipy
import json
from sklearn.model_selection import train_test_split
from tsfresh import extract_features, extract_relevant_features, select_features
from tsfresh.utilities.dataframe_functions import impute
from tsfresh.feature_extraction import MinimalFCParameters
import pandas as pd
from scipy import sparse

def parse_file(filename):

    dataset = pd.read_csv(filename, names=['time', 'rtt', 'id', 'label'])
    data = dataset[['time','rtt', 'id']]
    labels = dataset['label']

    return data, labels


def extract_labels_for_feature(filename, feature, cols=['time', 'rtt', 'id', 'outage', 'noise', 'congestion', 'changepoint']):
    """ Extract labels of a specific feature from the CSV. """
    dataset = pd.read_csv(filename, names=cols, header=0)
    data = dataset[['time','rtt','id']]
    labels = dataset[feature]

    return data, labels

def extract_feature_labels(filename, feature, cols=['time', 'rtt', 'outage', 'congestion', 'noise', 'changepoint']):
    """ Extract labels of a specific feature from the CSV. """
    dataset = pd.read_csv(filename, names=cols, header=0)
    data = dataset[['time','rtt']] # this wanted an id column-- not sure why?
    data['id'] = data['time']
    
    labels = dataset[feature]
    
    return data, labels


def split_data(X, data, y, num_test=500):
    np.random.seed(1234)
    num_sample = np.shape(X)[0]

    X_test = X[0:num_test,:]
    X_train = X[num_test:, :]
    
    data_train = data[num_test:]
    data_test = data[0:num_test]

    y_test = y[0:num_test]
    y_train = y[num_test:]

    # split dev/test
    test_ratio = 0.2
    X_tr, X_te, y_tr, y_te, data_tr, data_te = \
        train_test_split(X_train, y_train, data_train, test_size=test_ratio)

    return np.array(X_tr), np.array(X_te), np.array(X_test), \
        np.array(y_tr), np.array(y_te), np.array(y_test), data_tr, data_te, data_test


class DataLoader(object):
    """ A class to load in appropriate numpy arrays """

    def load_data(self, dataset, data_path):
        # Parse Files
        data, labels = parse_file(data_path+dataset)
        data['time'] = data['time'].apply(lambda x: str(x))
        
        extraction_settings = MinimalFCParameters()
        data_extracted_feats = extract_features(data, \
                     column_id='id', column_sort='time',\
                     default_fc_parameters=extraction_settings,\
                     impute_function= impute)


        X_list = data_extracted_feats.values.tolist()
        X = np.array(X_list)

        #Split Dataset into Train, Val, Test

        # note: train_ground will not be used
        train_primitive_matrix, val_primitive_matrix, test_primitive_matrix, \
            train_ground, val_ground, test_ground, \
            train_original, val_original, test_original = split_data(X, data, labels)
        
        return train_primitive_matrix, val_primitive_matrix, test_primitive_matrix, \
            np.array(train_ground), np.array(val_ground), np.array(test_ground), \
            train_original, val_original, test_original
    
    def load_feature(self, dataset, data_path, feature):
        """ Wrapper for extract_labels_for_feature. """
        # Parse Files
        data, labels = extract_labels_for_feature(data_path+dataset, feature)
        data['time'] = data['time'].apply(lambda x: str(x))
        
        extraction_settings = MinimalFCParameters()
        data_extracted_feats = extract_features(data, \
                     column_id='id', column_sort='time',\
                     default_fc_parameters=extraction_settings,\
                     impute_function=impute)

        X_list = data_extracted_feats.values.tolist()
        X = np.array(X_list)

        # Split Dataset into Train, Val, Test
        #   note: train_ground will not be used
        train_primitive_matrix, val_primitive_matrix, test_primitive_matrix, \
            train_ground, val_ground, test_ground, \
            train_original, val_original, test_original = split_data(X, data, labels)

        return train_primitive_matrix, val_primitive_matrix, test_primitive_matrix, \
            np.array(train_ground), np.array(val_ground), np.array(test_ground), \
            train_original, val_original, test_original
        
        
    def select_feature(self, dataset, data_path, feature, col_id='id'):
        """ Wrapper for extract_featuire_labels. """
        # Parse Files
        data, labels = extract_feature_labels(data_path + dataset, feature)
        data['time'] = data['time'].apply(lambda x: str(x))
        
#         print(data)
        
        extraction_settings = MinimalFCParameters()
        
#         print(data)
        
        data_extracted_feats = extract_features(data, column_id=col_id, column_sort='time', \
                                                default_fc_parameters=extraction_settings, impute_function= impute)

        X_list = data_extracted_feats.values.tolist()
        X = np.array(X_list)
        
#         print("X LIST:", len(X_list))
        
        # Split Dataset into Train, Val, Test
        #   note: train_ground will not be used
        train_primitive_matrix, val_primitive_matrix, test_primitive_matrix, \
            train_ground, val_ground, test_ground, \
            train_original, val_original, test_original = split_data(X, data, labels)

        return train_primitive_matrix, val_primitive_matrix, test_primitive_matrix, \
            np.array(train_ground), np.array(val_ground), np.array(test_ground), \
            train_original, val_original, test_original