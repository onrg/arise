import numpy as np
from keras.models import Sequential
from keras.layers import Dense, LSTM, Flatten, Dropout
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
from keras import optimizers, regularizers
from keras.callbacks import EarlyStopping, ModelCheckpoint
import matplotlib.pyplot as plt



def lstm_simple(train_text, y_train, test_text, y_test, client, heuristic, bs=64, n=3, num_cells=100, lr=0.001, l2_reg=0.01, dropout_val=0.2):
    #Label Processing
    y_train[y_train == -1] = 0
    y_test[y_test == -1] = 0

    
    
    X_train, y_train = np.array(train_text), np.array(y_train)
    X_test, y_test = np.array(test_text), np.array(y_test)
    X_train = np.reshape(X_train, (len(X_train), 1, 1))
    X_test = np.reshape(X_test, (len(X_test), 1, 1))

    length = 60
    
    X_train = sequence.pad_sequences(X_train, maxlen=length)
    
    X_test = sequence.pad_sequences(X_test, maxlen=length)
    

    #Model Architecture
    model = Sequential()
    
    model.add(
        LSTM(
            units=num_cells,
            input_shape=(X_train.shape[1],1), 
            bias_regularizer=regularizers.l2(l2_reg), 
            dropout=dropout_val
        )
    )
    
#     model.add(Dense(
#         units=num_cells, 
#         input_shape=(X_train.shape[1],1), 
#         activation='sigmoid',
#         bias_regularizer=regularizers.l2(l2_reg)
#     ))
    

    # --------------------- ADDING ADDITIONAL LAYERS -----
    # no additional layers -- baseline
    # uncomment the lines below to add additional hidden layers
    # to the STL models.
    
    # model.add(Dense(60, activation='relu')) #  3 total
    # model.add(Dense(60, activation='relu'))
    # model.add(Dense(60, activation='relu')) #  5 total
    # model.add(Dense(60, activation='relu'))
    # model.add(Dense(60, activation='relu'))
    # model.add(Dense(60, activation='relu'))
    # model.add(Dense(60, activation='relu'))
    # model.add(Dense(60, activation='relu')) # 10 total
    
    # --------------------- END ADDITIONAL LAYERS --------
    
    
    # model.add(Dropout(dropout_val))
    model.add(Dense(1, activation='sigmoid'))
    
    optimizer_model = optimizers.adam(learning_rate=lr, beta_1=0.9, beta_2=0.999)
    #Run the model!
    model.compile(loss='binary_crossentropy', optimizer=optimizer_model, metrics=['accuracy'])
    # print(model.summary())
    
    use_es = True # use early stopping?
    
    if use_es:
        es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, min_delta=0.01, patience=5)
        
        # mc = ModelCheckpoint(client + '-' + heuristic + '-best-model.h5', monitor='val_loss', mode='min', verbose=1, save_best_only=True)
        
        history = model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=n, batch_size=bs, callbacks=[es])
    else:
        history = model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=n, batch_size=bs)


    # print(history.history.keys())
    

    
    # summarize history for accuracy
    # plt.plot(history.history['accuracy'])
    # plt.plot(history.history['val_accuracy'])
    # plt.title('Model Accuracy #Units {}, #Epoch {}, Batch Size {}'.format(num_cells, n, bs))
    # plt.ylabel('Accuracy')
    # plt.xlabel('Epoch')
    # plt.legend(['Train', 'Val'], loc='upper left')
    # plt.tight_layout()
    # plt.show()

    """ summarize history for loss """
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model Loss #Units {}, #Epoch {}, Batch Size {}'.format(num_cells, n, bs))
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Val'], loc='upper right')
    plt.tight_layout()

    plt.show()


    scores = model.evaluate(X_test, y_test, verbose=0)
    print("Accuracy: %.2f%%" % (scores[1]*100))

    y_pred = model.predict(X_test, batch_size=1)
    y_pred = np.array([x[0] for x in y_pred])
    return y_pred
