# EMERGE Single-Task Learning

The single-task learning approach in this paper was implemented based on the code from the 2020 NetAI paper, "[Challenges in Using ML for Networking Research: How to Label If You Must](https://dl.acm.org/doi/10.1145/3405671.3405812)." 
The original source for this project is available at [https://gitlab.com/onrg/emerge](https://gitlab.com/onrg/emerge).

The following instructions are excerpted from the original EMERGE `README.md` file and were used to conduct the single-task learning experiments in the ARISE paper.

---------

EMERGE creates training labels for network data at scale and in a programmable fashion.
Users can either label a small portion of the dataset or write labeling functions to
have EMERGE learn from these inputs and assign labels to the data.

## Creating labels by providing labels for a small set of data

### Preprocess data
- Go to `root/data_28/`
- Run `python preprocess_data.py`
- Rename datasets 1-9 by adding '0' in front of the dataset number: dataset_1 -> dataset_01

### Randomize Data (optional)
- See `root/snuba/data/Randomize Data.ipynb` for details.

### Create thresholds
- Go to `root/snuba/data/`
- Record threshold values in CSV file as given the example in create_threshold.ipynb

### Create input for Snuba and generate probabilistic labels
- Go to `root/snuba/`
- Follow the tutorial in `input_snuba_generate_prob_labels.ipynb`

### Evaluate train label quality
- Go to `root/snuba/`
- Follow the tutorial in evaluate_label_quality_lstm.ipynb to run EMERGE discriminative model

## Creating labels by writing labeling functions
This pipeline can be used to collaborate with other groups when the collaborating groups cannot share raw data or ML models due to privacy concerns.

### Preprocess data
- Go to `root/data_28/`
- Run `python preprocess_data.py`
- Rename datasets 1-9 by adding '0' in front of the dataset number, e.g., `dataset_1` -> `dataset_01`

### Create input for NoMoNoise and record thresholds
- Go to `root/nomonoise/input_data_nomonoise/`
- Create NoMoNoise input and record threshold values in CSV file, follow `create_threshold.ipynb`

### Combine LFs and assess the label quality
- Go to `root/nomonoise/`
- Follow examples in `Combine_LFs.ipynb`
- Results can be found in `root/nomonoise/results/`

## Environment
Install Conda and Snorkel-Extraction:

`cd /tmp`
`curl -O https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh`
`bash Miniconda3-latest-Linux-x86_64.sh`
`cd ~`

### Clone the repository
`git clone https://github.com/snorkel-team/snorkel-extraction.git`
`cd snorkel-extraction`

### Create EMERGE environment and install the snorkel-extraction dependencies
`conda env create -n emerge --file=environment.yml`

### Activate the environment
`conda activate emerge_env`

### Install snorkel in the environment
`pip install .`

### Go to EMERGE directory
`cd ~/multitaskws`

### Install the rest of EMERGE dependencies
`pip install pathlib==1.0.1`
`pip install -r requirements.txt`


### Dependencies:
<p>Note that these only list the important dependencies. For a complete list of dependencies check <code>dependencies_compelete_list.txt</code>. <span style="color: red;">Authors' Note: this is superceded by the <code style="color:red;">arise-requirements.txt</code> file.</span></p>


```bash
- python==3.6.7
- snorkel==0.7.0b0
- pandas==0.23.0
- scikit-learn==0.22
- tensorflow==1.15.0
- Keras==2.3.1
- pytorch==1.1.0
- numpy==1.16.0
- tsfresh==0.11.2
- future==0.17.1
```

### Install virtual environment on Jupyter Notebook, then run it.
- `pip install --user ipykernel`
- `python -m ipykernel install --user --name=emerge_env`
- `jupyter-notebook`

(Use `jupyter-notebook --no-browser` if running the notebook headless.)
